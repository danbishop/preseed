#!/bin/ruby
require 'sinatra'

get '/' do
  'Preseed'
end

get '/preseed.cfg' do
    erb :preseed
end

get '/bootmenu.ipxe' do
  erb :bootmenu
end
